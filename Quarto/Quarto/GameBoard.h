#pragma once

#include "GamePiece.h"
#include <array>
#include <memory>
#include <stdint.h>

class GameBoard
{
public:
	GameBoard() = default;
	~GameBoard() = default;

	void generateNewGame();
	void printAvailablePieces() const;
	void printBoard() const;
	bool checkWin() const;
	void addPiece(const int8_t, const int8_t, const int8_t);

private:
	std::array<std::unique_ptr<GamePiece>, 16> m_piecesOnBoard;
	std::array<std::unique_ptr<GamePiece>, 16> m_piecesNotOnBoard;
};

