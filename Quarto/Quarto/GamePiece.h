#pragma once

#include <iostream>

class GamePiece
{
public:
	enum class Color : uint8_t {
		BLACK,
		WHITE
	};
	enum class Shape : uint8_t {
		SQUARE,
		ROUND
	};
	enum class Height : uint8_t {
		TALL,
		SHORT
	};
	enum class Middle : uint8_t {
		HOLE,
		FILL
	};

public:
	GamePiece(const Color, const Shape, const Height, const Middle);
	GamePiece(int8_t, int8_t, int8_t, int8_t);
	GamePiece(GamePiece&&) = default;
	~GamePiece() = default;

	bool operator==(const GamePiece&) const;
	friend std::ostream& operator<<(std::ostream&, const GamePiece&);

private:
	Color m_color : 1;
	Shape m_shape : 1;
	Height m_height : 1;
	Middle m_middle : 1;
};
