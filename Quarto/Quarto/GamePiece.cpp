#include <string>

#include "GamePiece.h"

GamePiece::GamePiece(const Color color, const Shape shape, const Height height, const Middle middle):
	m_color(color),
	m_shape(shape),
	m_height(height),
	m_middle(middle)
{
	// Empty constructor
}

GamePiece::GamePiece(int8_t color, int8_t shape, int8_t height, int8_t middle) {
	if (color == 0)
		m_color = GamePiece::Color::BLACK;
	else if (color == 1)
		m_color = GamePiece::Color::WHITE;
	if (shape == 0)
		m_shape = GamePiece::Shape::SQUARE;
	else if (shape == 1)
		m_shape = GamePiece::Shape::ROUND;
	if (height == 0)
		m_height = GamePiece::Height::TALL;
	else if (height == 1)
		m_height = GamePiece::Height::SHORT;
	if (middle == 0)
		m_middle = GamePiece::Middle::HOLE;
	else if (middle == 1)
		m_middle = GamePiece::Middle::FILL;
}

bool GamePiece::operator==(const GamePiece &other) const {
	if (this != nullptr && &other != nullptr)
		return m_color == other.m_color || m_shape == other.m_shape || m_height == other.m_height || m_middle == other.m_middle;
	return false;
}

std::ostream& operator<<(std::ostream &os, const GamePiece &piece) {
	if (piece.m_color == GamePiece::Color::BLACK)
		os << "Bl";
	else
		os << "Wh";
	if (piece.m_shape == GamePiece::Shape::SQUARE)
		os << "Sq";
	else
		os << "Ro";
	if (piece.m_height == GamePiece::Height::TALL)
		os << "Ta";
	else
		os << "Sh";
	if (piece.m_middle == GamePiece::Middle::HOLE)
		os << "Ho";
	else
		os << "Fi";
	return os;
}
