#include <string>
#include <bitset>

#include "GameBoard.h"

void GameBoard::generateNewGame() {
	for (int8_t pieceIndex = 0; pieceIndex < 16; ++pieceIndex) {
		m_piecesNotOnBoard[pieceIndex] = nullptr;
		m_piecesOnBoard[pieceIndex] = nullptr;
	}
	for (int8_t numberForBitset = 0; numberForBitset < 16; ++numberForBitset) {
		std::bitset<4> bits(numberForBitset);
		m_piecesNotOnBoard[numberForBitset] = std::make_unique<GamePiece>(bits[0], bits[1], bits[2], bits[3]);
	}
}

void GameBoard::printAvailablePieces() const {
	for (int8_t columnIndex = 0; columnIndex < 4; ++columnIndex) {
		for (int8_t lineIndex = 0; lineIndex < 4; ++lineIndex) {
			std::cout << lineIndex * 4 + columnIndex + 1 << ". ";
			if (m_piecesNotOnBoard[lineIndex * 4 + columnIndex] == nullptr)
				std::cout << "        ";
			else
				std::cout << *m_piecesNotOnBoard[lineIndex * 4 + columnIndex];
			std::cout << "  ";
		}
		std::cout << "\n";
	}
}

void GameBoard::printBoard() const {
	auto printLine = [=](const int8_t lineIndex) {
		std::string filledLine("+----------+----------+----------+----------+");
		std::cout << filledLine << "\n";
		for (int8_t i = lineIndex * 4; i < lineIndex * 4 + 4; ++i) {
			std::cout << "| ";
			if (m_piecesOnBoard[i] != nullptr)
				std::cout << *m_piecesOnBoard[i];
			else
				std::cout << "        ";
			std::cout << " ";
		}
		std::cout << "|\n";
	};
	for (int8_t lineIndex = 0; lineIndex < 4; ++lineIndex)
		printLine(lineIndex);
	std::cout << "+----------+----------+----------+----------+\n";
}

bool GameBoard::checkWin() const {
	for (int8_t lineIndex = 0; lineIndex < 4; ++lineIndex)
		if (*m_piecesOnBoard[lineIndex * 4 + 0] == *m_piecesOnBoard[lineIndex * 4 + 1] &&
			*m_piecesOnBoard[lineIndex * 4 + 1] == *m_piecesOnBoard[lineIndex * 4 + 2] &&
			*m_piecesOnBoard[lineIndex * 4 + 2] == *m_piecesOnBoard[lineIndex * 4 + 3])
			return true;
	for (int8_t columnIndex = 0; columnIndex < 4; ++columnIndex)
		if (*m_piecesOnBoard[0 + columnIndex] == *m_piecesOnBoard[4 + columnIndex] &&
			*m_piecesOnBoard[4 + columnIndex] == *m_piecesOnBoard[8 + columnIndex] &&
			*m_piecesOnBoard[8 + columnIndex] == *m_piecesOnBoard[12 + columnIndex])
			return true;
	if (*m_piecesOnBoard[0] == *m_piecesOnBoard[5] &&
		*m_piecesOnBoard[5] == *m_piecesOnBoard[10] &&
		*m_piecesOnBoard[10] == *m_piecesOnBoard[15])
			return true;
	if (*m_piecesOnBoard[3] == *m_piecesOnBoard[6] &&
		*m_piecesOnBoard[6] == *m_piecesOnBoard[9] &&
		*m_piecesOnBoard[9] == *m_piecesOnBoard[12])
			return true;
	return false;
}

void GameBoard::addPiece(const int8_t pieceIndex, const int8_t x, const int8_t y) {
	if (m_piecesNotOnBoard[pieceIndex] != nullptr && m_piecesOnBoard[4 * x + y] == nullptr)
		m_piecesOnBoard[4 * x + y] = move(m_piecesNotOnBoard[pieceIndex]);
	else
		std::cout << "Bad move\n";
}