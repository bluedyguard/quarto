#include "GameBoard.h"

#include <iostream>

void Quarto() {
	GameBoard quarto;
	quarto.generateNewGame();
	bool gameWon = false, player = true;
	int16_t pieceIndex, x, y;
	while (gameWon == false) {
		system("cls");
		player = !player;
		quarto.printAvailablePieces();
		quarto.printBoard();
		if (player == false)
			std::cout << "Player 1 chooses the piece for player 2.\n";
		else
			std::cout << "Player 2 chooses the piece for player 1.\n";
		std::cin >> pieceIndex;
		std::cout << "Where would you like to put it? (x, y)\n";
		std::cin >> x >> y;
		quarto.addPiece(pieceIndex - 1, x - 1, y - 1);
		gameWon = quarto.checkWin();
	}
	system("cls");
	if (player == true)
		std::cout << "Player 1 won.\n";
	else
		std::cout << "Player 2 won.\n";
}

int main() {
	Quarto();
	system("pause");
	return 0;
}
